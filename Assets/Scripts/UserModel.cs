using UnityEngine;
using Realms;
//using Newtonsoft.Json;
using System;
using MongoDB.Bson;

public class UserModel : RealmObject
{
    [PrimaryKey]
    [MapTo("_id")]
    public string ID { get; set; }

    [MapTo("_activity")]
    public string lastActivity { get; set; }

    [MapTo("_instances")]
    public int availableInstances { get; set; }

    public UserModel() { }

    public UserModel(string id)
    {
        ID = id;
        lastActivity = DateTime.UtcNow.ToJson();
        availableInstances = 3;
    }
}
