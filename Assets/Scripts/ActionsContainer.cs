using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionsContainer : MonoBehaviour
{
    private List<ActionUIItem> items = new List<ActionUIItem>();

    [SerializeField] private ActionItem actionItem_prefab;
    [SerializeField] private AddActionItem addActionItem_prefab;

    [SerializeField] private RectTransform container;

    private GameplayInstance instance;

    private AddActionItem addView;

    public void Setup(GameplayInstance instance)
    {
        this.instance = instance;

        SetDefaultActions();

        foreach (var item in instance.actions)
        {
            var ai = AddAction();

            ai.Setup(item, this);
        }
    }

    private void SetDefaultActions()
    {

        var action = instance.CreateAction();

        action.type = ActionState.ActionType.KEY;
        action.actionKey = KeyCode.F1;

        var conditions = new List<ActionCondition>();

        var condition = new ActionCondition();

        condition.target = ActionCondition.ConditionTarget.TARGET;
        condition.type = ActionCondition.ConditionType.HP;
        condition.relation = ActionCondition.ConditionRelation.MORE;
        condition.conditionValue = 0;

        conditions.Add(condition);

        action.conditions = conditions;

        action = instance.CreateAction();

        action.type = ActionState.ActionType.KEY;
        action.actionKey = KeyCode.F2;

        conditions = new List<ActionCondition>();

        condition = new ActionCondition();

        condition.target = ActionCondition.ConditionTarget.TARGET;
        condition.type = ActionCondition.ConditionType.HP;
        condition.relation = ActionCondition.ConditionRelation.LESS;
        condition.conditionValue = 0;

        conditions.Add(condition);

        action.conditions = conditions;
    }

    public ActionItem AddAction()
    {
        var newItem = Instantiate(actionItem_prefab);

        newItem.transform.SetParent(container);

        newItem.transform.localScale = Vector3.one;

        items.Add(newItem);

        UpdateActionsContainer();

        return newItem;
    }

    private void UpdateActionsContainer()
    {
        float width = items.Count * 100;

        if (addView != null) width += 100;

        Debug.Log(width);

        container.sizeDelta = new Vector2(width, 100);
    }

    public void RemoveAction(ActionItem item)
    {
        items.Remove(item);

        Destroy(item.gameObject);
    }

    private void CheckAddView()
    {
        if (items.Count < 3)
        {
            if (addView == null)
            {
                addView = Instantiate(addActionItem_prefab);
                addView.transform.SetParent(container);

                addView.Setup(this);

                addView.transform.localScale = Vector3.one;
            }
        }
        else
        {
            if (addView != null)
                Destroy(addView.gameObject);

            return;
        }

        if (addView != null)
        {
            addView.transform.SetAsLastSibling();
        }
    }
}

[Serializable]
public class ActionState
{
    public enum ActionType
    {
        NONE, KEY//, IN_GAME_ACTION
    }

    public bool active;

    public ActionType type;
    public KeyCode actionKey;
    //private string inGameAction;

    public float pressRate;

    public float nextCheck;

    public float occupiedTimeBefore;
    public float occupiedTimeAfter;

    public List<ActionCondition> conditions;


    public ActionState()
    {

    }

    public ActionCondition CreateCondition(ConditionsPanel panel)
    {
        var c = new ActionCondition();

        conditions.Add(c);

        panel.AddConditionView(c);

        return c;
    }


    public void RemoveCondition(ActionCondition condition)
    {
        conditions.Remove(condition);
    }

    public bool IsValid(GameplayInstance gameplayInstance)
    {
        if (type == ActionType.NONE) return false;

        foreach (var item in conditions)
        {
            switch (item.type)
            {
                case ActionCondition.ConditionType.HP:

                    switch (item.target)
                    {
                        case ActionCondition.ConditionTarget.THIS:

                            if (item.relation == ActionCondition.ConditionRelation.LESS)
                            {
                                if (gameplayInstance.hpValue > item.conditionValue)
                                    return false;
                                else
                                    continue;
                            }
                            else
                            {
                                if (gameplayInstance.hpValue <= item.conditionValue)
                                    return false;
                                else
                                    continue;
                            }

                            break;

                        case ActionCondition.ConditionTarget.TARGET:

                            if (item.relation == ActionCondition.ConditionRelation.LESS)
                            {
                                if (gameplayInstance.targetHpValue > item.conditionValue)
                                    return false;
                                else
                                    continue;
                            }
                            else
                            {
                                if (gameplayInstance.targetHpValue <= item.conditionValue)
                                    return false;
                                else
                                    continue;

                            }

                            break;

                        case ActionCondition.ConditionTarget.OTHER:

                            //TODO

                            break;
                    }

                    break;
                case ActionCondition.ConditionType.MP:

                    switch (item.target)
                    {
                        case ActionCondition.ConditionTarget.THIS:

                            if (item.relation == ActionCondition.ConditionRelation.LESS)
                                return !(gameplayInstance.mpValue <= item.conditionValue);
                            else
                                return !(gameplayInstance.mpValue > item.conditionValue);

                            break;

                        case ActionCondition.ConditionTarget.OTHER:

                            //TODO

                            break;
                    }
                    break;
                case ActionCondition.ConditionType.CP:

                    switch (item.target)
                    {
                        case ActionCondition.ConditionTarget.THIS:

                            if (item.relation == ActionCondition.ConditionRelation.LESS)
                                return !(gameplayInstance.cpValue <= item.conditionValue);
                            else
                                return !(gameplayInstance.cpValue > item.conditionValue);

                            break;

                        case ActionCondition.ConditionTarget.OTHER:

                            //TODO

                            break;
                    }
                    break;
            }
        }
        return true;
    }

}

[Serializable]
public class ActionCondition
{
    public enum ConditionType
    {
        HP, MP, CP
    }

    public enum ConditionRelation
    {
        MORE, LESS
    }

    public enum ConditionTarget
    {
        THIS, TARGET, OTHER
    }

    public ConditionType type;
    public ConditionRelation relation;
    public ConditionTarget target;
    public float conditionValue;
    public int otherNum;
    //public bool hasTarget = true;

    public ActionCondition()
    {
        type = ConditionType.HP;
        relation = ConditionRelation.MORE;
        target = ConditionTarget.TARGET;
    }
}

[Serializable]
public class ActionSavingContainer
{

}



