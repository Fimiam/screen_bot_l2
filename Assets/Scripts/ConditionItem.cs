using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ConditionItem : UIItem
{
    private ActionCondition actionCondition;

    [SerializeField] private Slider valueSlider;
    [SerializeField] private TextMeshProUGUI value_Text;
    [SerializeField] private Toggle toggleHP, toggleMP, toggleCP, toggleLess, togleMore, toggleMe, toggleTarget;

    public ActionCondition Condition => actionCondition;

    private ConditionsPanel panel;

    private void Awake()
    {
        transform.localScale = Vector3.zero;

        transform.DOScale(1, .25f);
    }

    public void Setup(ActionCondition actionCondition, ConditionsPanel conditionsPanel)
    {
        this.actionCondition = actionCondition;
        panel = conditionsPanel;

        SetView(actionCondition);
    }


    // Update is called once per frame
    void Update()
    {
        //actionCondition.conditionValue = valueSlider.value;

        value_Text.text = $"{Mathf.Floor(valueSlider.value * 100)}%";

        //SetType();
        //SetRelation();
        //SetTarget();
    }

    private void SetType()
    {
        actionCondition.type = toggleHP.isOn ? ActionCondition.ConditionType.HP :
            toggleMP.isOn ? ActionCondition.ConditionType.MP :
            ActionCondition.ConditionType.CP;
    }

    private void SetRelation()
    {
        actionCondition.relation = toggleLess.isOn ? ActionCondition.ConditionRelation.LESS :
            ActionCondition.ConditionRelation.MORE;
    }

    private void SetTarget()
    {
        actionCondition.target = toggleMe.isOn ? ActionCondition.ConditionTarget.THIS :
            ActionCondition.ConditionTarget.TARGET;
    }

    private void SetView(ActionCondition condition)
    {
        switch (condition.target)
        {
            case ActionCondition.ConditionTarget.THIS:

                toggleMe.isOn = true;

                break;
            case ActionCondition.ConditionTarget.TARGET:

                toggleTarget.isOn = true;

                break;
        }

        switch (condition.type)
        {
            case ActionCondition.ConditionType.HP:

                toggleHP.isOn = true;

                break;
            case ActionCondition.ConditionType.MP:

                toggleMP.isOn = true;

                break;
            case ActionCondition.ConditionType.CP:

                toggleCP.isOn = true;

                break;
        }

        switch (condition.relation)
        {
            case ActionCondition.ConditionRelation.MORE:

                togleMore.isOn = true;

                break;
            case ActionCondition.ConditionRelation.LESS:

                toggleLess.isOn = true;

                break;
        }

        valueSlider.value = condition.conditionValue;
    }

    public void RemoveClicked()
    {
        transform.DOScale(0, .2f).SetEase(Ease.Flash).onComplete += () => { panel.RemoveConditionItem(this); };
    }

}
