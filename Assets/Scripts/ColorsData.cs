using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ColorsData", menuName = "ScriptableObjects/ColorsData")]
public class ColorsData : ScriptableObject
{
    public int SOLID_HP_RED, RESTORING_HP_RED, SOLID_HP_TARGET;
    public int SOLID_CP_RED;
    public int SOLID_MP_BLUE;
}
