using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddConditionItem : UIItem
{
    [SerializeField] private Button button;

    private bool clicked;

    private ConditionsPanel panel;

    private void Awake()
    {
        button.transform.localScale = Vector3.zero;

        button.transform.DOScale(1, .25f);
    }

    public void Setup(ConditionsPanel conditionsPanel)
    {
        panel = conditionsPanel;
    }

    void Start()
    {
        button.onClick.AddListener(OnClick);
    }

    private void OnClick()
    {
        if (clicked) return;

        clicked = true;

        button.transform.DOScale(0, .2f).SetEase(Ease.Flash).onComplete += () => { panel.AddConditionItem(); };

        StartCoroutine(CheckIdle());
    }

    private IEnumerator CheckIdle()
    {
        yield return new WaitForSeconds(2f);

        clicked = false;

        button.transform.DOScale(1, .2f);
    }
}
