using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddActionItem : ActionUIItem
{
    private bool clicked;

    [SerializeField] private Button button;

    private ActionsContainer container;

    public void Setup(ActionsContainer actionsContainer)
    {
        container = actionsContainer;
    }

    private void OnClick()
    {
        if (clicked) return;

        clicked = true;

        button.transform.DOScale(0, .2f).SetEase(Ease.Flash).onComplete += () => { container.AddAction(); };

        StartCoroutine(CheckIdle());
    }

    private IEnumerator CheckIdle()
    {
        yield return new WaitForSeconds(2f);

        clicked = false;

        button.transform.DOScale(1, .2f);
    }
}
