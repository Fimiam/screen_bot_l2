using DG.Tweening;
using SFB;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameplayView : UIItem
{
    [SerializeField] private RawImage characterHUD_Image;
    [SerializeField] private Bar barCp, barHp, barMp;

    public RectTransform container;

    [SerializeField] private ActionsContainer actionsContainer;

    [SerializeField] private Button playButton, saveButton, loadButton, closeButton, rightMoveButton, leftMoveButton;

    [SerializeField] private TextMeshProUGUI order_text;

    [SerializeField] private GameObject moveView;
    [SerializeField] private RectTransform searchingView, gameplayControlView;

    private GameplayInstance instance;

    private string lastPath;

    [HideInInspector] public bool searchingState = true;

    private void Awake()
    {
        //container.SetAnchor(AnchorPresets.MiddleCenter);

        container.transform.localScale = Vector3.zero;

        container.DOScale(1, .2f);

        searchingState = true;

        searchingView.DOPunchScale(-Vector3.one * .2f, 2.1f, 2).SetLoops(-1);
    }

    public void Setup(GameplayInstance instance)
    {
        this.instance = instance;
        actionsContainer.Setup(instance);

        playButton.onClick.AddListener(PlayButtonClick);
        loadButton.onClick.AddListener(LoadButtonClick);
        saveButton.onClick.AddListener(SaveButtonClick);
        closeButton.onClick.AddListener(CloseButtonClick);
        rightMoveButton.onClick.AddListener(RightMoveClick);
        leftMoveButton.onClick.AddListener(LeftMoveClick);

        order_text.text = $"{transform.GetSiblingIndex()}";
    }


    private void Update()
    {
        if(searchingState && !instance.searchingTargetWindow)
        {
            searchingState = false;

            searchingView.DOKill();

            var seq = DOTween.Sequence();

            seq.Append(searchingView.DOScaleX(0, .21f));
            seq.Append(gameplayControlView.DOScaleX(1, .21f));
        }


        characterHUD_Image.texture = instance.hudTexture;

        barHp.SetTargetValue(instance.hpValue);
        barMp.SetTargetValue(instance.mpValue);
        barCp.SetTargetValue(instance.cpValue);
    }

    private void RightMoveClick()
    {
        transform.GetComponentInParent<InstancesView>().MoveView(this, true);
    }

    private void LeftMoveClick()
    {
        transform.GetComponentInParent<InstancesView>().MoveView(this, false);
    }

    public void UpdateMovingView(int order, bool rightActive, bool leftActive)
    {
        moveView.SetActive(leftActive || rightActive);

        if(!moveView.activeSelf)
        {
            return;
        }

        order_text.text = order.ToString();

        leftMoveButton.gameObject.SetActive(leftActive);
        rightMoveButton.gameObject.SetActive(rightActive);

    }

    private  void CloseButtonClick()
    {
        MainModule.Instance.RemoveGameplayInsance(instance);
        MainWindow.Instance.viewes.CloseView(this);
    }

    private void SaveButtonClick()
    {
        var actionsData = new ActionsSavingData();

        var temp = new ActionState[instance.actions.Count];

        instance.actions.CopyTo(temp);

        actionsData.actions = temp.ToList();

        var json = JsonUtility.ToJson(actionsData);

        var levelPath = StandaloneFileBrowser.SaveFilePanel(
            "Save actions data",
            lastPath,
            "data" + ".json",
            "json"
        );
        if (levelPath.Length != 0)
        {
            lastPath = levelPath;
            var data = json;
            if (data != null)
                File.WriteAllText(levelPath, data);
        }
    }
    private void LoadButtonClick()
    {
        var path = StandaloneFileBrowser.OpenFilePanel(
            "Choose data to load",
            lastPath,
            "json",
            false
        )[0];

        path = new Uri(path).LocalPath;

        var data = System.IO.File.ReadAllText(path);

        var ad = JsonUtility.FromJson<ActionsSavingData>(data);
    }
    private void PlayButtonClick()
    {
        instance.SwitchActive();

        var txt = playButton.transform.GetComponentInChildren<TextMeshProUGUI>();

        txt.text = instance.active ? "STOP" : "PLAY";
    }
}

[Serializable] 
public class ActionsSavingData
{
    public List<ActionState> actions;
}
