using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionUIItem : UIItem
{
    private float targetX;

    [SerializeField] private float moveSpeed = 10;

    private RectTransform rectTrans;

    private void Awake()
    {
        transform.localScale = Vector3.zero;

        transform.DOScale(1, .25f);
    }

    private void Start()
    {
        rectTrans = GetComponent<RectTransform>();
    }

    private void SetAction(ActionState action)
    {

    }

    public void SetTargetX(float value)
    {
        targetX = value;

        enabled = true;
    }
}
