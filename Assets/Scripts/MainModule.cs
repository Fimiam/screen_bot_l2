using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainModule : MonoBehaviour
{
    public static MainModule Instance;

    [SerializeField] private GameplayInstance gameplay_prefab;

    private List<GameplayInstance> instances = new List<GameplayInstance>();

    [SerializeField] private MainWindow window;

    private void Awake()
    {
        Instance = this;

        Application.targetFrameRate = 30;

#if !UNITY_EDITOR
        //window.SetNoBorders();
#endif
    }
    
    public void AddGameplayInstance(AddGameplayView addView)
    {
        var gameplay = Instantiate(gameplay_prefab);

        gameplay.transform.parent = transform;

        instances.Add(gameplay);

        gameplay.Setup();
    }

    public void RemoveGameplayInsance(GameplayInstance instance)
    {
        if(instances.Remove(instance))
        {
            Destroy(instance.gameObject);
        }
    }
}
