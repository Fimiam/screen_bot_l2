using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddGameplayView : UIItem
{
    [SerializeField] private Button button;

    private bool clicked = false;

    private void Awake()
    {
        button.transform.localScale = Vector3.zero;

        button.transform.DOScale(1, .25f);
    }

    public void AddViewClick()
    {
        if (clicked) return;

        clicked = true;

        button.transform.DOScale(0, .2f).SetEase(Ease.Flash).onComplete += () => { MainModule.Instance.AddGameplayInstance(this); };

        StartCoroutine(CheckIdle());
    }

    private IEnumerator CheckIdle()
    {
        yield return new WaitForSeconds(1f);

        clicked = false;

        button.transform.DOScale(1, .2f);
    }
}
