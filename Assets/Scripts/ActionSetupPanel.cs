using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionSetupPanel : MonoBehaviour
{
    public ActionState action;

    [SerializeField] private CanvasGroup group;

    [SerializeField] private RectTransform typesRect, toggleRect, myRect;
    [SerializeField] private ConditionsPanel conditionsPanel;

    public bool shown, inTransition;

    private Vector3 initialScale;

    private List<ConditionItem> conditions = new List<ConditionItem>();


    public void Setup(ActionState action)
    {
        this.action = action;

        initialScale = transform.localScale;

        conditionsPanel.Setup(this);
    }

    public void Open(bool rightAlign = true)
    {
        inTransition = true;

        if(rightAlign)
        {
            myRect.SetAnchor(AnchorPresets.TopLeft);

            myRect.pivot = Vector2.up;

            typesRect.SetAnchor(AnchorPresets.TopRight);
            toggleRect.SetAnchor(AnchorPresets.TopRight);

            typesRect.anchoredPosition = new Vector2(-74, -27);
            toggleRect.anchoredPosition = new Vector2(-50, -34.5f);
        }
        else
        {
            myRect.SetAnchor(AnchorPresets.TopRight);

            typesRect.SetAnchor(AnchorPresets.TopLeft);
            toggleRect.SetAnchor(AnchorPresets.TopLeft);

            typesRect.anchoredPosition = new Vector2(280, -27);
            toggleRect.anchoredPosition = new Vector2(15, -34.5f);
        }

        foreach (var item in FindObjectsOfType<ActionSetupPanel>())
        {
            if (item != this)
                item.Close();
        }

        transform.GetComponentInParent<ActionItem>().myCanvas.sortingOrder = 2;

        myRect.anchoredPosition = Vector2.zero;

        transform.localScale = initialScale;

        group.interactable = false;
        group.alpha = 0;

        gameObject.SetActive(true);

        var seq = DOTween.Sequence();

        seq.Append(transform.parent.parent.DOScale(1.5f, .2f));
        seq.Join(transform.DOScale(1, .2f));
        seq.Join(group.DOFade(1, .15f));


        seq.onComplete += () => 
        {
            group.interactable = true;
            group.alpha = 1;
            inTransition = false;
            shown = true;
        };
    }

    public void Close()
    {
        inTransition = true;

        var seq = DOTween.Sequence();

        seq.Append(transform.parent.parent.DOScale(1, .2f));
        seq.Join(transform.DOScale(initialScale, .2f));
        seq.Join(group.DOFade(.2f, .15f));
        seq.Append(group.DOFade(0f, .2f));

        seq.onComplete += () =>
        {
            group.interactable = false;
            gameObject.SetActive(false);
            inTransition = false;
            shown = false;

            transform.GetComponentInParent<ActionItem>().myCanvas.sortingOrder = 1;
        };
    }
}

public enum AnchorPresets
{
    TopLeft,
    TopCenter,
    TopRight,

    MiddleLeft,
    MiddleCenter,
    MiddleRight,

    BottomLeft,
    BottonCenter,
    BottomRight,
    BottomStretch,

    VertStretchLeft,
    VertStretchRight,
    VertStretchCenter,

    HorStretchTop,
    HorStretchMiddle,
    HorStretchBottom,

    StretchAll
}

public enum PivotPresets
{
    TopLeft,
    TopCenter,
    TopRight,

    MiddleLeft,
    MiddleCenter,
    MiddleRight,

    BottomLeft,
    BottomCenter,
    BottomRight,
}

public static class RectTransformExtensions
{
    public static void SetAnchor(this RectTransform source, AnchorPresets allign, int offsetX = 0, int offsetY = 0)
    {
        source.anchoredPosition = new Vector3(offsetX, offsetY, 0);

        switch (allign)
        {
            case (AnchorPresets.TopLeft):
                {
                    source.anchorMin = new Vector2(0, 1);
                    source.anchorMax = new Vector2(0, 1);
                    break;
                }
            case (AnchorPresets.TopCenter):
                {
                    source.anchorMin = new Vector2(0.5f, 1);
                    source.anchorMax = new Vector2(0.5f, 1);
                    break;
                }
            case (AnchorPresets.TopRight):
                {
                    source.anchorMin = new Vector2(1, 1);
                    source.anchorMax = new Vector2(1, 1);
                    break;
                }

            case (AnchorPresets.MiddleLeft):
                {
                    source.anchorMin = new Vector2(0, 0.5f);
                    source.anchorMax = new Vector2(0, 0.5f);
                    break;
                }
            case (AnchorPresets.MiddleCenter):
                {
                    source.anchorMin = new Vector2(0.5f, 0.5f);
                    source.anchorMax = new Vector2(0.5f, 0.5f);
                    break;
                }
            case (AnchorPresets.MiddleRight):
                {
                    source.anchorMin = new Vector2(1, 0.5f);
                    source.anchorMax = new Vector2(1, 0.5f);
                    break;
                }

            case (AnchorPresets.BottomLeft):
                {
                    source.anchorMin = new Vector2(0, 0);
                    source.anchorMax = new Vector2(0, 0);
                    break;
                }
            case (AnchorPresets.BottonCenter):
                {
                    source.anchorMin = new Vector2(0.5f, 0);
                    source.anchorMax = new Vector2(0.5f, 0);
                    break;
                }
            case (AnchorPresets.BottomRight):
                {
                    source.anchorMin = new Vector2(1, 0);
                    source.anchorMax = new Vector2(1, 0);
                    break;
                }

            case (AnchorPresets.HorStretchTop):
                {
                    source.anchorMin = new Vector2(0, 1);
                    source.anchorMax = new Vector2(1, 1);
                    break;
                }
            case (AnchorPresets.HorStretchMiddle):
                {
                    source.anchorMin = new Vector2(0, 0.5f);
                    source.anchorMax = new Vector2(1, 0.5f);
                    break;
                }
            case (AnchorPresets.HorStretchBottom):
                {
                    source.anchorMin = new Vector2(0, 0);
                    source.anchorMax = new Vector2(1, 0);
                    break;
                }

            case (AnchorPresets.VertStretchLeft):
                {
                    source.anchorMin = new Vector2(0, 0);
                    source.anchorMax = new Vector2(0, 1);
                    break;
                }
            case (AnchorPresets.VertStretchCenter):
                {
                    source.anchorMin = new Vector2(0.5f, 0);
                    source.anchorMax = new Vector2(0.5f, 1);
                    break;
                }
            case (AnchorPresets.VertStretchRight):
                {
                    source.anchorMin = new Vector2(1, 0);
                    source.anchorMax = new Vector2(1, 1);
                    break;
                }

            case (AnchorPresets.StretchAll):
                {
                    source.anchorMin = new Vector2(0, 0);
                    source.anchorMax = new Vector2(1, 1);
                    break;
                }
        }
    }

    public static void SetPivot(this RectTransform source, PivotPresets preset)
    {

        switch (preset)
        {
            case (PivotPresets.TopLeft):
                {
                    source.pivot = new Vector2(0, 1);
                    break;
                }
            case (PivotPresets.TopCenter):
                {
                    source.pivot = new Vector2(0.5f, 1);
                    break;
                }
            case (PivotPresets.TopRight):
                {
                    source.pivot = new Vector2(1, 1);
                    break;
                }

            case (PivotPresets.MiddleLeft):
                {
                    source.pivot = new Vector2(0, 0.5f);
                    break;
                }
            case (PivotPresets.MiddleCenter):
                {
                    source.pivot = new Vector2(0.5f, 0.5f);
                    break;
                }
            case (PivotPresets.MiddleRight):
                {
                    source.pivot = new Vector2(1, 0.5f);
                    break;
                }

            case (PivotPresets.BottomLeft):
                {
                    source.pivot = new Vector2(0, 0);
                    break;
                }
            case (PivotPresets.BottomCenter):
                {
                    source.pivot = new Vector2(0.5f, 0);
                    break;
                }
            case (PivotPresets.BottomRight):
                {
                    source.pivot = new Vector2(1, 0);
                    break;
                }
        }
    }
}
