using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScreenData", menuName = "ScriptableObjects/ScreenData")]
public class GamescreenData : ScriptableObject
{
    public Vector2Int WINDOW_BORDERS_SIZE;
    public Vector2Int HUD_OFFSET;
    public Vector2Int HUD_SIZE;
    public Vector2Int HP_OFFSET;
    public Vector2Int MP_OFFSET;
    public Vector2Int CP_OFFSET;
    public Vector2Int TARGET_HP_MIDDLE_OFFSET;
    public int CHARACTER_STAT_WIDTH, TARGET_HP_WIDTH;
}
