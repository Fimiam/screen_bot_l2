using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class WindowControls : MonoBehaviour, IDragHandler
{
    public void OnDrag(PointerEventData data)
    {
        if (BorderlessWindow.framed)
            return;

        MainWindow.Instance._deltaValue += data.delta;

        if (data.dragging)
        {
            BorderlessWindow.MoveWindowPos(MainWindow.Instance._deltaValue, Screen.width, Screen.height);
        }
    }
}
