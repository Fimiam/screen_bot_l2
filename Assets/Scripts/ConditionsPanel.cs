using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConditionsPanel : MonoBehaviour
{
    [SerializeField] private ConditionItem conditionItem_prefab;
    [SerializeField] private AddConditionItem addItem_prefab;
    [SerializeField] private Transform itemsContainer;

    private ActionSetupPanel panel;

    private List<UIItem> items = new List<UIItem>();

    public void Setup(ActionSetupPanel setupPanel)
    {
        panel = setupPanel;

        CheckAddItem();
    }

    public void AddConditionItem()
    {
        panel.action.CreateCondition(this);

        CheckAddItem();
    }

    private void CheckAddItem()
    {
        if (items.Exists(i => i is AddConditionItem) || items.Count > 2) return;

        var item = Instantiate(addItem_prefab);

        item.transform.SetParent(itemsContainer);

        items.Add(item);

        item.transform.SetParent(itemsContainer);
    }

    public void AddConditionView(ActionCondition condition)
    {
        var addItem = items.Find(i => i is AddConditionItem);

        if (addItem == null) return;

        var item = Instantiate(conditionItem_prefab);

        item.Setup(condition, this);

        items.Add(item);

        item.transform.SetParent(itemsContainer);
    }

    public void RemoveConditionItem(ConditionItem conditionItem)
    {
        panel.action.RemoveCondition(conditionItem.Condition);

        items.Remove(conditionItem);

        Destroy(conditionItem.gameObject);
    }
}
