using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ActionItem : ActionUIItem
{
    [SerializeField] private TextMeshProUGUI actionText;
    [SerializeField] private ActionSetupPanel setupPanel;
    [SerializeField] private Button button, removeButton;
    public Canvas myCanvas;

    public void Setup(ActionState action, ActionsContainer actionsContainer)
    {
        setupPanel.Setup(action);

        actionText.text = action.actionKey.ToString().ToUpper();

        button.onClick.AddListener(OnClick);
    }

    private void OnClick()
    {
        var sp = Camera.main.WorldToScreenPoint(transform.position);

        if (!setupPanel.inTransition)
        {
            if (setupPanel.shown)
            {
                setupPanel.Close();
            }
            else
            {
                setupPanel.Open(((float)sp.x / (float)Screen.width) < .6f);
            }
        }
    }
    
    private void RemoveClick()
    {

    }
}
