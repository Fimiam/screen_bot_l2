using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Drawing.Text;
using UnityEngine;

public class InstancesView : MonoBehaviour
{
    private List<UIItem> items = new List<UIItem>();

    [SerializeField] private GameplayView gameplayView_prefab;
    [SerializeField] private AddGameplayView addView_prefab;

    [SerializeField] private Transform container;

    private AddGameplayView addView;

    private void Start()
    {

    }

    public void AddGameplayView(GameplayInstance gameplay)
    {
        foreach (var item in FindObjectsOfType<ActionSetupPanel>())
        {
            item.Close();
        }

        CreateGameplayView(gameplay);
    }

    public void CloseView(UIItem view)
    {

        view.transform.DOScale(0, .2f).onComplete += () =>
        {
            items.Remove(view);

            Destroy(view.gameObject);

            for (int i = 0; i < items.Count; i++)
            {
                var item = items[i] as GameplayView;

                item.UpdateMovingView(i, i < items.Count - 1, i > 0);
            }
        };
    }

    private void Update()
    {
        CheckAddView();
    }

    private void CheckAddView()
    {
        if (items.Count < 3)
        {
            if(addView == null)
            {
                for (int i = 0; i < items.Count; i++)
                {
                    var item = items[i] as GameplayView;

                    if (item.searchingState) return;
                }

                addView = Instantiate(addView_prefab);
                addView.transform.SetParent(container);

                addView.transform.localScale = Vector3.one;
            }
            else
            {
                for (int i = 0; i < items.Count; i++)
                {
                    var item = items[i] as GameplayView;

                    if (item.searchingState)
                    {
                        Destroy(addView.gameObject);
                        addView = null;

                        return;
                    }
                }
            }
        }
        else
        {
            if(addView != null)
                Destroy(addView.gameObject);

            return;
        }

        if (addView != null)
        {
            addView.transform.SetAsLastSibling();
        }
    }
    
    private GameplayView CreateGameplayView(GameplayInstance instance)
    {
        var newView = Instantiate(gameplayView_prefab);

        newView.transform.SetParent(container);

        newView.transform.localScale = Vector3.one;

        newView.transform.SetSiblingIndex(items.Count);

        Debug.Log("41");

        items.Add(newView);

        Debug.Log("42");

        newView.Setup(instance);

        Debug.Log("45");

        for (int i = 0; i < items.Count; i++)
        {
            var item = items[i] as GameplayView;

            item.UpdateMovingView(i, i < items.Count - 1, i > 0);
        }

        return newView;
    }

    public void MoveView(GameplayView view, bool right)
    {
        float spacing = 72 , size = 460; // config ?

        var viewIndex = items.IndexOf(view);

        var toIndex = right ? viewIndex + 1 : viewIndex - 1;

        if (toIndex < 0 || toIndex > items.Count - 1) return;

        var toView = items[toIndex] as GameplayView;

        var offset = Vector2.right * (spacing + size);

        view.transform.SetSiblingIndex(toIndex);
        toView.transform.SetSiblingIndex(viewIndex);

        view.container.anchoredPosition = right ? -offset : offset;
        toView.container.anchoredPosition = right ? offset : -offset;

        var seq = DOTween.Sequence();

        seq.Append(view.container.DOAnchorPos(Vector2.zero, .34f));
        seq.Join(toView.container.DOAnchorPos(Vector2.zero, .34f));

        items[toIndex] = view;
        items[viewIndex] = toView;

        for (int i = 0; i < items.Count; i++)
        {
            var item = items[i] as GameplayView;

            item.UpdateMovingView(i, i < items.Count - 1, i > 0);
        }
    }
}
