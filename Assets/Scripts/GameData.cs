using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData : MonoBehaviour
{
    public static GameData Instance;

    public GamescreenData screenData;
    public ColorsData colorsData;

    private void Awake()
    {
        Instance = this;
    }

}
