//using MongoDB.Driver;
//using MongoDB.Bson;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Realms;
using Realms.Sync;
using System;
using MongoDB.Bson;
using DeviceId;
using DeviceId.Components;

public class DBManager : MonoBehaviour
{
    public static DBManager Instance;

    //private MongoClient client;

    //private IMongoDatabase database;

    private Realm realm;
    private UserModel userModel;

    [SerializeField] private string REALM_ID;

    private App app;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        //realm.RemoveAll();

        StartCoroutine(DB());
    }

    private IEnumerator DB()
    {
        var deviceID = new DeviceIdBuilder().
        OnWindows(windows =>
        windows.AddMotherboardSerialNumber()).
        ToString();

        app = App.Create(REALM_ID);

        yield return app.LogInAsync(Credentials.Anonymous());

        var conf = new RealmConfiguration()
        {
            ShouldDeleteIfMigrationNeeded = true,
            SchemaVersion = 0

        };

        var realmTask = Realm.GetInstanceAsync(conf);

        yield return realmTask;

        realm = realmTask.Result;

        userModel = realm.Find<UserModel>(deviceID);

        if (userModel == null)
        {
            realm.Write(() =>
            {
                userModel = realm.Add(new UserModel(deviceID));
            });
        }
        else
        {
            realm.Write(() =>
            {
                userModel.lastActivity = DateTime.UtcNow.ToJson();
            });
        }
    }

    private void OnDisable()
    {
        if(realm != null)
            realm.Dispose();
    }


}
