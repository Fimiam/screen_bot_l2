using System;
using System.Windows;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using UnityEngine;
using Random = UnityEngine.Random;
using WindowsInput;
using WindowsInput.Native;
using UnityEngine.UI;

public class GameplayInstance : MonoBehaviour
{
    private IntPtr targetWindowHandle = IntPtr.Zero;

    private Bitmap actualBitmap;

    private System.Drawing.Graphics gfxBmp;

    private IntPtr hdcBitmap;

    public bool searchingTargetWindow;

    public Texture2D hudTexture, hpTexture, mpTexture, cpTexture, targetTexture;

    public float hpValue, mpValue, cpValue, targetHpValue;

    private RECT activeWindowRect = default;

    private float nextShoot, shootRate = .066f;

    public bool active = false;

    private float toNextAction;

    [SerializeField] private float defaultActionsRate = .12f;

    private float randomRate = .02f;

    public List<ActionState> actions = new List<ActionState>();

    private int lastAction;

    private InputSimulator inputSimulator;

    private Coroutine shootingRoutine;

    private void Awake()
    {
        SetSearchingState();

        inputSimulator = new InputSimulator();
    }

    public void SwitchActive() => active = !active;

    public ActionState CreateAction()
    {
        var action = new ActionState();

        actions.Add(action);

        return action;
    }

    public void Setup()
    {
        MainWindow.Instance.viewes.AddGameplayView(this); 
    }

    private void Start()
    {
        //StartCoroutine(Shooting());
    }

    // Update is called once per frame
    void Update()
    {
        if(searchingTargetWindow)
        {
            var lastHandle = targetWindowHandle;

            targetWindowHandle = ProcessesCatcher.GetForegroundWindow();

            if(lastHandle != targetWindowHandle)
            {
                SetupGraphics();
            }

            CheckShot();

            if(cpValue > .02f && hpValue > .15f && mpValue > .03f)
            {
                searchingTargetWindow = false;

                ResetGameUI();

                if (shootingRoutine == null) shootingRoutine = StartCoroutine(Shooting());
            }

            return;
        }

        if(active)
            UpdateActions();
    }

    private void UpdateActions()
    {
        toNextAction -= Time.deltaTime;

        if (toNextAction < 0 && actions.Count > 0)
        {
            lastAction++;

            lastAction = lastAction > actions.Count - 1 ? 0 : lastAction;

            var action = actions[lastAction];

            Debug.Log("trying action = " + lastAction);

            if (!action.IsValid(this)) return;

            ProceedAction(action);

            toNextAction = defaultActionsRate + Random.Range(-randomRate, randomRate);
        }
    }

    private bool IsActionValid(ActionState action)
    {
        foreach (var item in action.conditions)
        {
            switch (item.type)
            {
                case ActionCondition.ConditionType.HP:

                    switch (item.target)
                    {
                        case ActionCondition.ConditionTarget.THIS:





                            break;
                        case ActionCondition.ConditionTarget.TARGET:
                            break;
                    }

                    break;
                case ActionCondition.ConditionType.MP:
                    break;
                case ActionCondition.ConditionType.CP:
                    break;
            }
        }

        return true;
    }

    private void ProceedAction(ActionState action)
    {
        if (ProcessesCatcher.GetForegroundWindow() != targetWindowHandle)
        {
            ProcessesCatcher.SetForegroundWindow(targetWindowHandle);
        }

        VirtualKeyCode key = default;

        string toParse = action.actionKey.ToString();

        if (toParse.Contains("VK_"))
        {
            toParse = toParse.Split('_')[1];
        }

        Enum.TryParse(toParse, out key);

        inputSimulator.Keyboard.KeyPress(key);
    }

    private void UpdateShooting()
    {
        bool haveWindow = targetWindowHandle != IntPtr.Zero && !searchingTargetWindow;

        nextShoot -= Time.deltaTime;

        if (!haveWindow) return;

        if (nextShoot < 0)
        {
            DoShot();

            actualBitmap.RotateFlip(RotateFlipType.RotateNoneFlipY);

            hudTexture = GetHUDTexture();
            hpTexture = GetHPTexture();
            mpTexture = GetMPTexture();
            cpTexture = GetCPTexture();
            targetTexture = GetTargetTexture();

            StatsTexturesToValues();

            nextShoot = shootRate;
        }
    }

    private void ResetGameUI()
    {
        inputSimulator.Keyboard.KeyDown(VirtualKeyCode.LMENU);
        inputSimulator.Keyboard.KeyPress(VirtualKeyCode.VK_L);
        inputSimulator.Keyboard.KeyUp(VirtualKeyCode.LMENU);
    }

    private void CheckShot()
    {
        DoShot();

        actualBitmap.RotateFlip(RotateFlipType.RotateNoneFlipY);

        hudTexture = GetHUDTexture();
        hpTexture = GetHPTexture();
        mpTexture = GetMPTexture();
        cpTexture = GetCPTexture();
        targetTexture = GetTargetTexture();

        StatsTexturesToValues();
    }

    private void SetSearchingState()
    {
        searchingTargetWindow = true;
    }

    private Texture2D GetCPTexture()
    {
        Vector2Int frameOffset = GameData.Instance.screenData.WINDOW_BORDERS_SIZE;

        Vector2Int cpOffset = GameData.Instance.screenData.CP_OFFSET;

        var texture = new Texture2D(GameData.Instance.screenData.CHARACTER_STAT_WIDTH, 1, TextureFormat.RGB24, false);

        Color32 colorUnity = default;
        System.Drawing.Color colorBm = default;

        for (int y = 0; y < texture.height; y++)
        {
            for (int x = 0; x < texture.width; x++)
            {
                colorBm = actualBitmap.GetPixel(x + cpOffset.x + frameOffset.x, frameOffset.x + cpOffset.y + y);

                colorUnity.r = colorBm.R;
                colorUnity.g = colorBm.G;
                colorUnity.b = colorBm.B;

                texture.SetPixel(x, y, colorUnity);
            }
        }

        texture.Apply();

        return texture;
    }

    private Texture2D GetMPTexture()
    {
        Vector2Int frameOffset = GameData.Instance.screenData.WINDOW_BORDERS_SIZE;

        Vector2Int mpOffset = GameData.Instance.screenData.MP_OFFSET;

        var texture = new Texture2D(GameData.Instance.screenData.CHARACTER_STAT_WIDTH, 1, TextureFormat.RGB24, false);

        Color32 colorUnity = default;
        System.Drawing.Color colorBm = default;

        for (int y = 0; y < 1; y++)
        {
            for (int x = 0; x < texture.width; x++)
            {
                colorBm = actualBitmap.GetPixel(x + frameOffset.x + mpOffset.x, y + frameOffset.x + mpOffset.y);

                colorUnity.r = colorBm.R;
                colorUnity.g = colorBm.G;
                colorUnity.b = colorBm.B;

                texture.SetPixel(x, y, colorUnity);
            }
        }

        texture.Apply();

        return texture;
    }

    private Texture2D GetHPTexture() 
    {
        Vector2Int frameOffset = GameData.Instance.screenData.WINDOW_BORDERS_SIZE;

        Vector2Int hpRect = new Vector2Int(GameData.Instance.screenData.CHARACTER_STAT_WIDTH, 1);

        Vector2Int hpOffset = GameData.Instance.screenData.HP_OFFSET;

        var texture = new Texture2D(hpRect.x, hpRect.y, TextureFormat.RGB24, false);

        Color32 colorUnity = default;
        System.Drawing.Color colorBm = default;

        for (int y = 0; y < hpRect.y; y++)
        {
            for (int x = 0; x < hpRect.x; x++)
            {
                colorBm = actualBitmap.GetPixel(x + hpOffset.x + frameOffset.x, y + hpOffset.y + frameOffset.x);

                colorUnity.r = colorBm.R;
                colorUnity.g = colorBm.G;
                colorUnity.b = colorBm.B;

                texture.SetPixel(x, y, colorUnity);
            }
        }

        texture.Apply();

        return texture;
    }

    private Texture2D GetHUDTexture()
    {
        Vector2Int hudRect = GameData.Instance.screenData.HUD_SIZE;
        Vector2Int frameOffset = GameData.Instance.screenData.WINDOW_BORDERS_SIZE;
        Vector2Int hudOffset = GameData.Instance.screenData.HUD_OFFSET;

        var texture = new Texture2D(hudRect.x, hudRect.y, TextureFormat.RGB24, false);

        Color32 colorUnity = default;
        System.Drawing.Color colorBm = default;

        for (int y = 0; y < hudRect.y; y++)
        {
            for (int x = 0; x < hudRect.x; x++)
            {
                colorBm = actualBitmap.GetPixel(x + frameOffset.x + hudOffset.x, y + frameOffset.y + hudOffset.y);

                colorUnity.r = colorBm.R;
                colorUnity.g = colorBm.G;
                colorUnity.b = colorBm.B;

                texture.SetPixel(x, y, colorUnity);
            }
        }

        texture.Apply();

        return texture;
    }

    private Texture2D GetTargetTexture()
    {
        Vector2Int targetRect = new Vector2Int(GameData.Instance.screenData.TARGET_HP_WIDTH, 1);
        Vector2Int frameOffset = GameData.Instance.screenData.WINDOW_BORDERS_SIZE;
        Vector2Int targetMidOffset = GameData.Instance.screenData.TARGET_HP_MIDDLE_OFFSET;

        var noBordersBitmapSize = new Vector2Int(actualBitmap.Width - frameOffset.x * 2, actualBitmap.Height - (frameOffset.x + frameOffset.y));

        var texture = new Texture2D(targetRect.x, targetRect.y, TextureFormat.RGB24, false);

        Color32 colorUnity = default;
        System.Drawing.Color colorBm = default;

        int blackAreaWidth = 0;

        for (int x = actualBitmap.Width - 20; x > 0; x--)
        {
            colorBm = actualBitmap.GetPixel(x, 20);

            colorUnity.r = colorBm.R;
            colorUnity.g = colorBm.G;
            colorUnity.b = colorBm.B;

            if (colorBm.R == 0 && colorBm.G == 0 && colorBm.B == 0)
            {
                blackAreaWidth++;
            }
            else
            {
                break;
            }
        }

        if (blackAreaWidth > 30)
            blackAreaWidth += frameOffset.x * 2;
        else
            blackAreaWidth = 0;

        var startReadtargetHealth = noBordersBitmapSize.x / 2;

        var screenOddCompentassionOffset = noBordersBitmapSize.x % 2 == 0 ? -1 : 0;

        for (int y = 0; y < targetRect.y; y++)
        {
            for (int x = 0; x < targetRect.x; x++)
            {
                colorBm = actualBitmap.GetPixel(screenOddCompentassionOffset + startReadtargetHealth + x + frameOffset.x + (targetMidOffset.x - blackAreaWidth / 2), frameOffset.x + y + targetMidOffset.y);

                colorUnity.r = colorBm.R;
                colorUnity.g = colorBm.G;
                colorUnity.b = colorBm.B;

                texture.SetPixel(x, y, colorUnity);
            }
        }

        texture.Apply();

        return texture;
    }

    private IEnumerator Shooting()
    {
        while(true)
        {

            if (targetWindowHandle != IntPtr.Zero)
            {
                DoShot();

                yield return null;

                actualBitmap.RotateFlip(RotateFlipType.RotateNoneFlipY);

                hudTexture = GetHUDTexture();
                hpTexture = GetHPTexture();
                mpTexture = GetMPTexture();
                cpTexture = GetCPTexture();
                targetTexture = GetTargetTexture();

                StatsTexturesToValues();
            }

            yield return null;

        }
    }

    private void StatsTexturesToValues()
    {
        //hp

        int lastPositive = 0;

        var solidHpRed = GameData.Instance.colorsData.RESTORING_HP_RED;
        var solidMPBlue = GameData.Instance.colorsData.SOLID_MP_BLUE;
        var solidCPRed = GameData.Instance.colorsData.SOLID_CP_RED;
        var solidTargetHp = GameData.Instance.colorsData.SOLID_HP_TARGET;

        for (int x = GameData.Instance.screenData.CHARACTER_STAT_WIDTH - 1; x > -1; x--)
        {
            Color32 c = hpTexture.GetPixel(x, 0);

            if(c.g < 60)
            {
                if (c.r > solidHpRed - 5)
                {
                    lastPositive = x;
                    break;
                }
            }

        }

        if (lastPositive > 10) lastPositive++;

        hpValue = (float)lastPositive / (float)GameData.Instance.screenData.CHARACTER_STAT_WIDTH;

        hpValue = hpValue > 1.0f ? 0 : hpValue;

        //mp

        for (int x = GameData.Instance.screenData.CHARACTER_STAT_WIDTH - 1; x > -1; x--)
        {
            Color32 c = mpTexture.GetPixel(x, 0);

            if (c.r < 60)
            {
                if (c.b > solidMPBlue - 10)
                {
                    lastPositive = x;

                    break;
                }
            }

        }

        if (lastPositive > 10) lastPositive+=2;

        mpValue = (float)lastPositive / (float)GameData.Instance.screenData.CHARACTER_STAT_WIDTH;

        mpValue = mpValue > 1.0f ? 0 : mpValue;

        //cp

        for (int x = GameData.Instance.screenData.CHARACTER_STAT_WIDTH - 1; x > -1; x--)
        {
            Color32 c = cpTexture.GetPixel(x, 0);

            if (c.b < 10)
            {
                if (c.r > solidCPRed - 10)
                {
                    lastPositive = x;

                    break;
                }
            }

        }

        if (lastPositive > 10) lastPositive += 2;

        cpValue = (float)lastPositive / (float)GameData.Instance.screenData.CHARACTER_STAT_WIDTH;

        cpValue = cpValue > 1.0f ? 0 : cpValue;

        //target hp

        for (int x = GameData.Instance.screenData.TARGET_HP_WIDTH - 1; x > -1; x--)
        {
            Color32 c = targetTexture.GetPixel(x, 0);

            if (c.b < 60)
            {
                if (c.r > solidTargetHp - 10)
                {
                    lastPositive = x;

                    break;
                }
            }

        }

        if (lastPositive > 10) lastPositive ++;

        targetHpValue = (float)lastPositive / (float)GameData.Instance.screenData.TARGET_HP_WIDTH;

        targetHpValue = targetHpValue > 1.0f ? 0 : targetHpValue;
    }

    private void DoShot()
    {
        gfxBmp = System.Drawing.Graphics.FromImage(actualBitmap);
        hdcBitmap = gfxBmp.GetHdc();

        ProcessesCatcher.PrintWindow(targetWindowHandle, hdcBitmap, 0);

        ReleaseGraphics();
    }

    private void SetupGraphics()
    {
        ProcessesCatcher.GetWindowRect(targetWindowHandle, out activeWindowRect);

        // BITMAP HEIGHT MAYBE IN THE CONFIGS
        actualBitmap = new Bitmap(activeWindowRect.Width, 128, PixelFormat.Format24bppRgb); // BITMAP HEIGHT MAYBE IN THE CONFIGS
        // BITMAP HEIGHT MAYBE IN THE CONFIGS
    }

    private void ReleaseGraphics()
    {
        gfxBmp.ReleaseHdc(hdcBitmap);
        gfxBmp.Dispose();
    }

    private void OnDestroy()
    {
        ReleaseGraphics();
        actualBitmap.Dispose();
    }
}
