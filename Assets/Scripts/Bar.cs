using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bar : MonoBehaviour
{
    [SerializeField] private RectTransform backRect, fillRect;

    private float targetValue;

    private float value;

    private float maxFillWidth;

    [SerializeField] private float minSmooth = .5f, maxSmooth = .1f;

    void Start()
    {
        maxFillWidth = backRect.sizeDelta.x;
    }

    void Update()
    {
        value = Mathf.MoveTowards(value, targetValue, Mathf.Lerp(minSmooth, maxSmooth, targetValue));


        var size = fillRect.sizeDelta;

        size.x = value * maxFillWidth;

        fillRect.sizeDelta = size;


        if (value == targetValue) enabled = false;
    }

    public void SetTargetValue(float value)
    {
        targetValue = value;

        enabled = true;
    }
}
