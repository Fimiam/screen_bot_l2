﻿using System;
using System.Collections;
using System.Drawing.Printing;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.EventSystems;

public class MainWindow : MonoBehaviour
{

    public static MainWindow Instance;

    public Vector2Int defaultWindowSize;
    public Vector2Int borderSize;

    public Vector2 _deltaValue = Vector2.zero;
    private bool _maximized;

    public InstancesView viewes;

    private float screenRatio = 1.777778f;
    private float fromResolution = .3f;

    private Vector2Int targetSize, currentSize;

    private float lastResolutinHeight;

    private Coroutine windowSizingRoutine;

    private void Awake()
    {
        Instance = this;

        //lastResolutinHeight = Screen.currentResolution.height;
        targetSize = currentSize = new Vector2Int(Screen.width, Screen.height);
    }

    private void Start()
    {
        var mrgs = new ProcessesCatcher.MARGINS { cxLeftWidth = -1 };

        ProcessesCatcher.DwmExtendFrameIntoClientArea(ProcessesCatcher.GetActiveWindow(), ref mrgs);

#if !UNITY_EDITOR
        BorderlessWindow.SetFramelessWindow();
#endif
    }

    private void Update()
    {
        //if(Screen.currentResolution.height != lastResolutinHeight)
        //{
        //    lastResolutinHeight = Screen.currentResolution.height;

        //    int targetHeight = Mathf.FloorToInt(Screen.currentResolution.height * fromResolution);

        //    targetSize = new Vector2Int(Mathf.FloorToInt(targetHeight * screenRatio), targetHeight);

        //    if(windowSizingRoutine != null)
        //    {
        //        StopCoroutine(windowSizingRoutine);
        //    }

        //    windowSizingRoutine = StartCoroutine(Resizing());
        //}
    }

    private IEnumerator Resizing()
    {
        float duration = .4f, t = 0;

        Vector2Int initilSize = currentSize;

        while(t < duration)
        {
            t += Time.deltaTime;


            int x = (int)Mathf.Lerp(initilSize.x, targetSize.x, t / duration);
            int y = (int)Mathf.Lerp(initilSize.y, targetSize.y, t / duration);

            currentSize = new Vector2Int(x, y);

            Screen.SetResolution(currentSize.x, currentSize.y, FullScreenMode.Windowed);

            yield return null;
        }

        windowSizingRoutine = null;
    }

    public void ClickOnClose()
    {
        Application.Quit();
    }

#if !UNITY_EDITOR
    public void SetNoBorders()
    {
        if (!BorderlessWindow.framed)
            return;

        BorderlessWindow.SetFramelessWindow();
        BorderlessWindow.MoveWindowPos(Vector2Int.zero, Screen.width - borderSize.x, Screen.height - borderSize.y);
    }

    //public void OnMinimizeBtnClick()
    //{
    //    EventSystem.current.SetSelectedGameObject(null);
    //    BorderlessWindow.MinimizeWindow();
    //}

    //public void OnMaximizeBtnClick()
    //{
    //    EventSystem.current.SetSelectedGameObject(null);

    //    if (_maximized)
    //        BorderlessWindow.RestoreWindow();
    //    else
    //        BorderlessWindow.MaximizeWindow();

    //    _maximized = !_maximized;
    //}
#endif
}
