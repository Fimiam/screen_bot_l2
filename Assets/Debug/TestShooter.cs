using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;
using System.Windows;

public class TestShooter : MonoBehaviour
{
    [SerializeField] private RectTransform imageRect;
    [SerializeField] private RawImage image, healthImage, targetHealthImage, debugTex;
    private IntPtr targetWindowHandle = IntPtr.Zero;
    private Bitmap actualBitmap;
    private System.Drawing.Graphics gfxBmp;
    private IntPtr hdcBitmap;

    [SerializeField] private Vector2Int bordersSize, healthOffset, mpOffset, cpOffset, targetHealthMiddleScreenOffset;
    [SerializeField] private Vector2Int readHpOffset, readMpOffset, readCpOffset, readTargetHpOffset;

    [SerializeField] private Vector2Int hudOffset, hudSize; 

    private RECT activeWindowRect = default;

    private const int nChars = 256;
    private StringBuilder windowNameBuff = new StringBuilder(nChars);

    private void Start()
    {
        UnityEngine.Application.targetFrameRate = 30;   
    }

    // Update is called once per frame
    void Update()
    {
        if(targetWindowHandle == IntPtr.Zero)
        {
            var activeHandle = ProcessesCatcher.GetForegroundWindow();
            var myHandle = Process.GetCurrentProcess().MainWindowHandle;

            ProcessesCatcher.GetWindowText(activeHandle, windowNameBuff, nChars);

            if(windowNameBuff.ToString().Contains("Asterios"))
            {
                targetWindowHandle = activeHandle;
            }

            return;
        }

        SetupGraphics();

        DoShot();

        actualBitmap.RotateFlip(RotateFlipType.RotateNoneFlipY);

        Color32 colorUnity = default;
        System.Drawing.Color colorBm = default;

        int blackAreaWidth = 0;

        for (int x = actualBitmap.Width - 20; x > 0; x--)
        {
            colorBm = actualBitmap.GetPixel(x, 20);

            colorUnity.r = colorBm.R;
            colorUnity.g = colorBm.G;
            colorUnity.b = colorBm.B;

            if(colorBm.R == 0 && colorBm.G == 0 && colorBm.B == 0)
            {
                blackAreaWidth++;
            }
            else
            {
                break;
            }
        }

        if (blackAreaWidth > 30)
            blackAreaWidth += bordersSize.x * 2;
        else
            blackAreaWidth = 0;

        var noBordersBitmapSize = new Vector2Int(actualBitmap.Width - bordersSize.x * 2, actualBitmap.Height - (bordersSize.x + bordersSize.y));

        var texture = new Texture2D(noBordersBitmapSize.x, noBordersBitmapSize.y, TextureFormat.RGB24, false);

        var healthTexture = new Texture2D(153, 10, TextureFormat.RGB24, false);

        var targetHealthTexture = new Texture2D(150, 1, TextureFormat.RGB24, false);

        var hudTexture = new Texture2D(hudSize.x, hudSize.y);

        for (int y = 0; y < texture.height; y++)
        {
            for (int x = 0; x < texture.width; x++)
            {
                colorBm = actualBitmap.GetPixel(bordersSize.x + x, bordersSize.x + y);

                colorUnity.r = colorBm.R;
                colorUnity.g = colorBm.G;
                colorUnity.b = colorBm.B;

                texture.SetPixel(x, y, colorUnity);
            }
        }

        for (int y = 0; y < healthTexture.height; y++)
        {
            for (int x = 0; x < healthTexture.width; x++)
            {
                colorBm = actualBitmap.GetPixel(bordersSize.x + x + mpOffset.x, bordersSize.x + y + mpOffset.y);

                colorUnity.r = colorBm.R;
                colorUnity.g = colorBm.G;
                colorUnity.b = colorBm.B;

                healthTexture.SetPixel(x, y, colorUnity);
            }
        }

        var startReadtargetHealth = noBordersBitmapSize.x / 2;

        var screenOddCompentassionOffset = noBordersBitmapSize.x % 2 == 0 ? -1 : 0;

        for (int y = 0; y < targetHealthTexture.height; y++)
        {
            for (int x = 0; x < targetHealthTexture.width; x++)
            {
                colorBm = actualBitmap.GetPixel(screenOddCompentassionOffset + startReadtargetHealth + bordersSize.x + x + (targetHealthMiddleScreenOffset.x - blackAreaWidth / 2), bordersSize.x + y + targetHealthMiddleScreenOffset.y + readTargetHpOffset.y);

                colorUnity.r = colorBm.R;
                colorUnity.g = colorBm.G;
                colorUnity.b = colorBm.B;

                targetHealthTexture.SetPixel(x, y, colorUnity);
            }
        }

        for (int y = 0; y < hudTexture.height; y++)
        {
            for (int x = 0; x < hudTexture.width; x++)
            {
                colorBm = actualBitmap.GetPixel(bordersSize.x + x + hudOffset.x, bordersSize.x + y + hudOffset.y);

                colorUnity.r = colorBm.R;
                colorUnity.g = colorBm.G;
                colorUnity.b = colorBm.B;

                hudTexture.SetPixel(x, y, colorUnity);
            }
        }

        hudTexture.Apply();
        healthTexture.Apply();
        texture.Apply();
        targetHealthTexture.Apply();

        image.texture = texture;
        healthImage.texture = healthTexture;
        targetHealthImage.texture = targetHealthTexture;
        debugTex.texture = hudTexture;

        imageRect.sizeDelta = new Vector2(texture.width, texture.height);
    }

    private void DoShot()
    {

        gfxBmp = System.Drawing.Graphics.FromImage(actualBitmap);
        hdcBitmap = gfxBmp.GetHdc();

        ProcessesCatcher.PrintWindow(targetWindowHandle, hdcBitmap, 0);

        //WINDOWINFO info = default;

        //ProcessesCatcher.GetWindowInfo(targetWindowHandle, ref info);

        ReleaseGraphics();
    }

    private void SetupGraphics()
    {
        ProcessesCatcher.GetWindowRect(targetWindowHandle, out activeWindowRect);

        actualBitmap = new Bitmap(activeWindowRect.Width, 128, PixelFormat.Format24bppRgb);
    }

    private void ReleaseGraphics()
    {
        gfxBmp.ReleaseHdc(hdcBitmap);
        gfxBmp.Dispose();
    }
}
